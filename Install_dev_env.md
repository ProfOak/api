# Set up a development environment

This project is built by using **Poetry**. The recommended IDE is **pycharm**. The data validation logic is 
implemented using **Spark**. Although the latest version of **PySpark** contains required jar, we recommend you 
to install a fully functional spark on your PC to avoid many issues. Below shows the general steps you need to 
follow to set up a development environment. 

1. Install Spark
2. Install Python
3. Install Poetry
4. Install PyCharm
5. Clone the project
6. Set up the project config
7. Init the database of the application
8. Launch the main ui
9. Apply Tests


## 1. Install spark

### Linux  
To install spark on Linux, please follow this [tutorial](https://github.com/pengfei99/PySparkCommonFunc/blob/main/docs/Install_spark_on_debian.md).

### Windows 

To install spark on Windows, please follow this [tutorial](https://github.com/pengfei99/PySparkCommonFunc/blob/main/docs/install_spark_on_windows.md)
If you don't.

Note : Take the latest version of Apache Spark and the latest version of hadoop for the dependencies.

## 2. Install python

You will need to install python according to the **python version in the poetry config** './pyproject.toml'.

For Windows, download python from this [site](https://www.python.org/downloads/).

## 3. Install Poetry

Poetry is used to **manage the project dependencies**. Here are the steps to install it:

**Linux** :

1. Open Terminal :
    
    > $ curl -sSL https://install.python-poetry.org | python3 -
   
    Depending on the python installed, you may replace 'python3' with 'python' or 'py'.
    

2. To verify the installation, run the following command:

    > $ poetry --version


**Windows** :

1. Open Terminal :

    Windows (Anaconda prompt, WSL):
    
    > $ curl -sSL https://install.python-poetry.org | python3 -
    
    or Windows (Powershell):
    
    > $ (Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py -
    
    Depending on the python installed, you may replace 'python3' with 'python' or 'py'.


2. The installation script will suggest adding the folder with the poetry executable to the PATH variable. 
Do that by running the following command:

    > $Env:Path += ";C:\Users\USERNAME\AppData\Roaming\Python\Scripts"; setx PATH "$Env:Path"
   
    Don't forget to replace USERNAME with your username!<br><br>
    
    **OR**<br><br>

    You can also add it manually in the window tab "Modifier les variables d'environnement système":
    
    1. Click on search toolbar, and type "environment"
    2. Select the result "Edit the system environment variables"
    3. A System Properties dialog box appears. In the lower-right corner, click "Environment Variables".
    4. In the top box, click on the "Path" entry, then click "Edit". 
    5. Then add the 'C:\Users\USERNAME\AppData\Roaming\pypoetry\venv\Scripts' to the Path


3. To verify the installation, run the following command:

    > $ poetry --version


For more information, visit the [Poetry documentation](https://python-poetry.org/docs/) and 
the [Poetry guide from PyCharm](https://www.jetbrains.com/help/pycharm/poetry.html).

## 4. Install PyCharm

PyCharm is the recommended IDE for this project.

To install Pycharm, please follow this [tutorial](https://www.jetbrains.com/help/pycharm/installation-guide.html).

Note for anaconda environment:<br>
    
    You can meet this error while loading the poetry environment : 'import _ssl DLL load fail error'
    
    This error is caused by the missing/misplacement of libcrypto file in anaconda3/DLLs folder:
    
        From anaconda3\Library\bin copy below files and paste them in anaconda3/DLLs:
        - libcrypto-1_1-x64.dll
        - libssl-1_1-x64.dll 

## 5. Clone the project

Use the following command to clone the project:

> $ git clone https://github.com/pengfei99/RecetteConstance.git

## 6. Set up the project config

The project config is located in '**./app/conf/config.ini**'. (template : 'config.ini.template').
You will have to configure the location of the logging configuration file and 
the directory where the application logs will be saved.
Then you will have to configure the Database (The type of database being used (e.g. sqlite, mysql, etc.)) and Spark.

Example for the path configuration :

    [APP]
    LOG_DIR = C:/Users/USERNAME/Documents/recette-data
    LOG_CONF = C:/Users/USERNAME/Documents/RecetteConstance/app/conf/logging.ini
    
    [DATABASE]
    DB_TYPE=sqlite
    PATH= C:/Users/USERNAME/Documents/recette-data/db.data

## 7. Initialize the database of the application

To **initialize the database** of the application, run the python file : './app/utils/initDB.py'

## 8. Launch the main ui

To launch the **main UI** of the application, run the python file : './app/main_ui.py'

This should start the application and display the main user interface.

## 9. Apply tests

Use the framework **Pytest** to test the code. The tests are stored in directory './app/tests'