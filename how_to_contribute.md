# Contribute to the project

## Set up a development environment

To contribute to this project, you need to install a development environment first. Follow this [tutorial](./Install_dev_env.md)
to set up a development environment for this project.

## Code style

PEP 8 and CamoCase.
