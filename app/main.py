import logging
import os
import pathlib
from fastapi import FastAPI, Request, Depends, HTTPException, status
from fastapi.openapi.docs import get_swagger_ui_html
from fastapi.middleware.cors import CORSMiddleware
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from passlib.context import CryptContext
from sqlalchemy.orm import sessionmaker
from datetime import datetime, timedelta
from pydantic import BaseModel
from typing import Annotated

from app.core.config_manager import ConfigManager
from app.core.log_manager import LogManager

from app.core.db.db_manager import DBManager

logger = logging.getLogger(__name__)

confFilePath = pathlib.Path.cwd() / "app" / "conf" / "config.ini"

configManager = ConfigManager(confFilePath)

LogManager(configManager)
logger.info("Starting app")

logger.info(f"Config manager init successful with config file {confFilePath}")
logger.info("Logger manager init successful")

SECRET_KEY = os.environ.get("SECRET_HASH_KEY")
print(SECRET_KEY)
ALGORITHM = os.environ.get("ALGORITHM")
ACCESS_TOKEN_EXPIRE_MINUTES = 30


class RepasResponse(BaseModel):
    res_id: int
    restaurant_name: str
    user_full_name: str
    hour: str


class RestauResponse(BaseModel):
    res_id: int
    name: str
    style: str
    city: str
    address: str
    price: int
    evaluation: float


class Repas(BaseModel):
    res_id: int
    hour: str


class Restaurant(BaseModel):
    name: str
    style: str
    city: str
    address: str
    price: int
    evaluation: float


class User(BaseModel):
    username: str
    full_name: str | None = None
    disabled: bool | None = None


class UserAuth(BaseModel):
    username: str
    password: str
    full_name: str | None = None


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str | None = None


class UserInDB(User):
    id: int
    hashed_password: str


class RestaurantInDB(Restaurant):
    id: int


class Evaluation(BaseModel):
    res_id: int
    mark: int


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

app = FastAPI(title="CASDALLE", version=0.1)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


dbManager = DBManager(configManager)
logger.info("DB manager init succesful")


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


def get_user(username: str):
    Session = sessionmaker(bind=dbManager.getEngine())
    session = Session()
    result = dbManager.getUserByName(session=session, username=username)
    if len(result) == 1:
        user = result[0]
        return UserInDB(
            id=user.id,
            username=user.username,
            full_name=user.full_name,
            disabled=user.disabled,
            hashed_password=user.hashed_password,
        )


def authenticate_user(username: str, password: str):
    user = get_user(username)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user


def create_access_token(data: dict, expires_delta: timedelta | None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def get_current_user(token: Annotated[str, Depends(oauth2_scheme)]):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        raise credentials_exception
    user = get_user(username=token_data.username)
    if user is None:
        raise credentials_exception
    return user


async def get_current_active_user(
    current_user: Annotated[User, Depends(get_current_user)]
):
    if current_user.disabled:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user


@app.post("/signup", summary="Create new user")
async def create_user(data: UserAuth):
    Session = sessionmaker(bind=dbManager.getEngine())
    session = Session()
    user = dbManager.getUserByName(session=session, username=data.username)
    if len(user) != 0:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="User with this username already exist",
        )
    else:
        dbManager.postUser(
            session,
            username=data.username,
            full_name=data.full_name,
            hashed_password=get_password_hash(data.password),
            disabled=False,
        )


@app.post("/token/", response_model=Token)
async def login_for_access_token(
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()]
):
    user = authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


@app.get("/")
async def show_welcome_page():
    message = {"Message": "Welcome to CASDalle API ! What are you even doing here ?"}
    return message


@app.get("/users/me/", response_model=User)
async def read_users_me(
    current_user: Annotated[UserInDB, Depends(get_current_active_user)]
):
    return current_user


@app.get("/where/")
async def get_all_repas():
    Session = sessionmaker(bind=dbManager.getEngine())
    session = Session()
    result = dbManager.getRepasToday(session, datetime.today())
    response = [
        RepasResponse(
            res_id=restaurant.id,
            restaurant_name=restaurant.name,
            user_full_name=user.full_name,
            hour=repas.hour,
        )
        for restaurant, user, repas in result
    ]

    session.close()
    return response


@app.get("/res/")
def get_resto(id):
    Session = sessionmaker(bind=dbManager.getEngine())
    session = Session()
    result = dbManager.getRestaurant(session=session, res_id=id)
    response = [
        RestauResponse(
            res_id=restaurant.id,
            name=restaurant.name,
            style=restaurant.style,
            city=restaurant.city,
            address=restaurant.address,
            price=restaurant.price,
            evaluation=restaurant.avg_mark,
        )
        for restaurant in result
    ]

    return response[0]


@app.get("/all_res/")
async def get_all_restos():
    result = dbManager.getAllRestaurant()
    response = []
    for restaurant in result:
        if not (restaurant.avg_mark):
            evaluation = -1
        else:
            evaluation = restaurant.avg_mark
        response.append(
            RestauResponse(
                res_id=restaurant.id,
                name=restaurant.name,
                style=restaurant.style,
                city=restaurant.city,
                address=restaurant.address,
                price=restaurant.price,
                evaluation=evaluation,
            )
        )
    return response


@app.post("/users/me/here/")
async def post_repas(
    current_user: Annotated[UserInDB, Depends(get_current_active_user)], repas: Repas
):
    Session = sessionmaker(bind=dbManager.getEngine())
    session = Session()
    if dbManager.getRepasUser(
        session=session,
        user_id=current_user.id,
        jour=datetime.today().strftime("%Y-%m-%d"),
    ):
        dbManager.updateRepas(
            session=session,
            restaurant_id=repas.res_id,
            user_id=current_user.id,
            jour=datetime.today().strftime("%Y-%m-%d"),
            hour=repas.hour,
        )

    else:
        dbManager.postRepas(
            session=session,
            restaurant_id=repas.res_id,
            user_id=current_user.id,
            jour=datetime.today().strftime("%Y-%m-%d"),
            hour=repas.hour,
        )


@app.post("/new_res/")
def post_resto(
    current_user: Annotated[UserInDB, Depends(get_current_active_user)],
    restaurant: Restaurant,
):
    Session = sessionmaker(bind=dbManager.getEngine())
    session = Session()
    dbManager.postRestaurant(
        session,
        restaurant.name,
        restaurant.style,
        restaurant.city,
        restaurant.address,
        restaurant.price,
    )


@app.post("/up_res/")
def maj_resto(
    current_user: Annotated[UserInDB, Depends(get_current_active_user)],
    restaurant: RestaurantInDB,
):
    Session = sessionmaker(bind=dbManager.getEngine())
    session = Session()
    dbManager.updateRestaurant(session, restaurant)


@app.post("/evaluate/")
def evaluate_resto(
    current_user: Annotated[UserInDB, Depends(get_current_active_user)],
    evaluation: Evaluation,
):
    Session = sessionmaker(bind=dbManager.getEngine())
    session = Session()

    if dbManager.getUserEvaluationRestaurant(
        session=session, user_id=current_user.id, restaurant_id=evaluation.res_id
    ):
        dbManager.updateUserEvaluationRestaurant(
            session=session,
            user_id=current_user.id,
            restaurant_id=evaluation.res_id,
            mark=evaluation.mark,
        )
    else:
        dbManager.postEvaluation(
            session=session,
            user_id=current_user.id,
            restaurant_id=evaluation.res_id,
            mark=evaluation.mark,
        )
