from sqlalchemy.orm import relationship, Mapped, mapped_column, column_property
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Boolean
from sqlalchemy import select, func
from typing import List

Base = declarative_base()

class User(Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    username = Column(String(255), nullable=False, unique=True)
    full_name = Column(String(255), nullable=False)
    hashed_password = Column(String(255), nullable=False)
    disabled = Column(Boolean, nullable=False)


class Evaluation(Base):
    __tablename__ = 'evaluation'
    restaurant_id = Column(Integer, ForeignKey(
        'restaurant.id'), primary_key=True)
    user_id = Column(Integer, ForeignKey(
        'user.id'), primary_key=True)
    mark = Column(Integer)
    restaurant = relationship("Restaurant")
    user = relationship("User")


class Restaurant(Base):
    __tablename__ = 'restaurant'
    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    style = Column(String(255), default="Inconnu")
    city = Column(String(255), default="Malakoff")
    address = Column(String(255), default="")
    price = Column(Integer, default=2)
    avg_mark = column_property(
        select(func.avg(Evaluation.mark))
        .where(Evaluation.restaurant_id == id)
    )


class Repas(Base):
    __tablename__ = 'restaurant_user'
    restaurant_id = Column(Integer, ForeignKey(
        'restaurant.id'), primary_key=True)
    user_id = Column(Integer, ForeignKey(
        'user.id'), primary_key=True)
    jour = Column(DateTime, primary_key=True)
    hour = Column(String(255), primary_key=True)
    restaurant = relationship("Restaurant")
    user = relationship("User")
