import json
import logging
from typing import List, Any, Optional, Type, Dict, Tuple

import pandas as pd
from pandas import DataFrame
from datetime import datetime
from sqlalchemy import create_engine, and_, text, desc, or_
from sqlalchemy.orm import sessionmaker
from app.core.config_manager import ConfigManager
from app.core.db.db_model import Restaurant, Repas, User, Evaluation

import os

logger = logging.getLogger(__name__)


class DBManager:
    EXT_VAL_RULE_TYPE_ID = 0
    DS_VAL_RULE_TYPE_ID = 1
    DB_POOL_SIZE = 40

    def __init__(self, configManager: ConfigManager):
        self.dbType = configManager.getDBType()
        self.dbUrl = configManager.getDBUrl()
        self.dbSize = int(configManager.getDBPoolSize())
        self.engine = self._buildEngine()

    def _buildEngine(
        self,
    ):
        if self.dbType == "sqlite":
            # 'sqlite:///C:\\path\\to\\foo.db'
            return create_engine(
                f"sqlite:///{self.dbUrl}", pool_size=self.dbSize, echo=True
            )
        elif self.dbType == "mysql":
            # 'mysql+mysqldb://scott:tiger@localhost:3306/foo'
            return create_engine(f"mysql+mysqldb://{self.dbUrl}", echo=True)
        elif self.dbType == "postgresql":
            # 'postgresql+psycopg2://scott:tiger@localhost:5432/foo'
            print(self.dbUrl)
            return create_engine(f"postgresql+psycopg2://{self.dbUrl}", echo=True)
        else:
            raise NotImplementedError

    def getEngine(self):
        return self.engine

    def getSession(self):
        Session = sessionmaker(bind=self.engine)
        return Session()

    def insertRow(self, row):
        result = False
        session = self.getSession()
        try:
            session.add(row)
            session.commit()
            result = True
        except Exception as e:
            logger.warning(f"Unable to insert row with exception {e}")
        finally:
            session.close()
            return result

    def removeRow(self, row):
        """
        Remove a row in a table
        :param row:
        :return:
        """
        result = False
        session = self.getSession()
        try:
            session.delete(row)
            session.commit()
            result = True
        except Exception as e:
            logger.warning(f"Unable to remove row with exception {e}")
        finally:
            session.close()
            return result

    def executeRawSqlQuery(self, sqlQuery: str) -> DataFrame:
        """
        This function takes a raw sql text, then run it in a sqlalchemy engine. At last, it converts the result
        into a pandas dataframe
        :param sqlQuery:
        :return:
        """
        conn = self.engine.connect()
        query = conn.execute(text(sqlQuery))
        df = pd.DataFrame(query.fetchall())
        conn.close()
        return df

    def getRows(self, objName) -> Any:
        session = self.getSession()
        rows = session.query(objName).all()
        session.close()
        return rows

    def getAllRestaurant(self):
        restaurantList = self.getRows(Restaurant)
        items = []
        for restaurant in restaurantList:
            items.append(restaurant)
        return items

    def getRepasToday(self, session, jour):
        jour = jour.date()
        result = (
            session.query(Restaurant, User, Repas)
            .filter(Repas.jour >= jour)
            .filter(
                Restaurant.id == Repas.restaurant_id,
            )
            .filter(
                User.id == Repas.user_id,
            )
            .all()
        )
        return result

    def getRestaurant(self, session, res_id):
        result = session.query(Restaurant).filter(Restaurant.id == res_id).all()
        return result

    def getUser(self, session, us_id):
        result = session.query(User).filter(User.id == us_id).all()
        return result

    def getUserByName(self, session, username):
        result = session.query(User).filter(User.username == username).all()
        return result

    def postUser(self, session, username, full_name, hashed_password, disabled):
        new_user = User(
            username=username,
            full_name=full_name,
            hashed_password=hashed_password,
            disabled=disabled,
        )
        session.add(new_user)
        session.commit()

    def postRestaurant(self, session, name, style, city, address, price):
        new_restaurant = Restaurant(
            name=name, style=style, city=city, address=address, price=price
        )
        session.add(new_restaurant)
        session.commit()

    def postRepas(self, session, restaurant_id, user_id, jour, hour):
        new_meal = Repas(
            restaurant_id=restaurant_id, user_id=user_id, jour=jour, hour=hour
        )
        session.add(new_meal)
        session.commit()

    def postEvaluation(self, session, user_id, restaurant_id, mark):
        evaluation = Evaluation(restaurant_id=restaurant_id, user_id=user_id, mark=mark)
        session.add(evaluation)
        session.commit()

    def getAllUserEvaluation(self, session, user_id):
        result = session.query(Evaluation).filter(Evaluation.user_id == user_id).all()
        return result

    def getUserEvaluationRestaurant(self, session, user_id, restaurant_id):
        result = (
            session.query(Evaluation)
            .filter(
                Evaluation.user_id == user_id,
            )
            .filter(
                Evaluation.restaurant_id == restaurant_id,
            )
            .all()
        )
        if len(result) == 0:
            return None
        else:
            return result[0]

    def updateUserEvaluationRestaurant(self, session, user_id, restaurant_id, mark):
        session.query(Evaluation).filter(
            Evaluation.user_id == user_id,
        ).filter(
            Evaluation.restaurant_id == restaurant_id,
        ).update({"mark": mark})
        session.commit()

    def getRepasUser(self, session, user_id, jour):
        jour = jour
        result = (
            session.query(Repas)
            .filter(Repas.jour >= jour)
            .filter(
                Repas.user_id == user_id,
            )
            .all()
        )
        print(result)
        if len(result) == 0:
            return None
        else:
            return result[0]

    def updateRepas(self, session, restaurant_id, user_id, jour, hour):
        session.query(Repas).filter(
            Repas.user_id == user_id,
        ).update({"jour": jour, "hour": hour, "restaurant_id": restaurant_id})
        session.commit()
