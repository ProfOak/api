import pytest

from app.core.config_manager import ConfigManager


@pytest.fixture
def configManagerSqlite():
    return ConfigManager("./data/config_sqlite.ini")


@pytest.fixture
def configManagerPostgres():
    return ConfigManager("./data/config_postgres.ini")


@pytest.fixture
def configManagerEmpty():
    return ConfigManager("./data/config_empty.ini")


def test_createConfigManager():
    cm = ConfigManager("./data/config_sqlite.ini")
    assert cm


def test_createConfigManager_withBadFormat():
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        ConfigManager("./data/config_bad.ini")
        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1


def test_showAllConfig_withValues(configManagerSqlite):
    configManagerSqlite.showAllConfig()


def test_showAllConfig_withEmptyValues(configManagerEmpty):
    configManagerEmpty.showAllConfig()


def test_getLogOutputDir(configManagerSqlite):
    expectedLogPath = "path/to/logs"
    actualLogPath = configManagerSqlite.getLogOutputDir()
    assert expectedLogPath == actualLogPath


def test_getLogOutputDir_withEmptyVal(configManagerEmpty):
    expectedPath = "path/to/logs"
    actPath = configManagerEmpty.getLogOutputDir()
    assert expectedPath == actPath


def test_getLogOutputDir_withBadVal(configManagerEmpty):
    expectedPath = "path/to/logs"
    actPath = configManagerEmpty.getLogOutputDir()
    assert expectedPath == actPath


def test_getLogConf_withEmptyVal(configManagerEmpty):
    expectedPath = "path/to/app/conf/config.ini"
    actualPath = configManagerEmpty.getLogConfPath()
    assert actualPath == expectedPath


def test_invalidConfigFile():
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        ConfigManager("config_postgres.ini")
    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 1
