import requests

url = "http://127.0.0.1:8000/AllRes"  # Remplacez cette URL par votre URL cible

# Envoyer la requête POST
response = requests.get(url)

# Vérifier le code de statut de la réponse
if response.status_code == 200:
    # Succès ! La requête a été traitée avec succès
    print("La requête GET a réussi !")
    print("Réponse du serveur:", response.json())
else:
    # Quelque chose s'est mal passé
    print("La requête GET a échoué. Code de statut:", response.status_code)
    print("Erreur :", response.text)
