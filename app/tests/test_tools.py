import pytest

from app.utils.tools import listContains


def test_listContains_notTrue():
    List1 = ['a', 'e', 'i', 'o', 'u']
    List2 = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']
    assert not listContains(List1, List2)


def test_listContains_True():
    subList = ['a', 'e', 'i']
    testList = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']
    assert listContains(subList, testList)
