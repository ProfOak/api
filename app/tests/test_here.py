import requests

url = "http://127.0.0.1:8000/Here"  # Remplacez cette URL par votre URL cible

# Les données que vous souhaitez envoyer dans la requête POST
data = {"res_id":"1", "us_id":"1"}

# Facultatif : si vous avez besoin d'envoyer des en-têtes personnalisés
headers = {
    "Content-Type": "application/json"
    #"Authorization": "Bearer votre_token"
}

# Envoyer la requête POST
response = requests.post(url, json=data, headers=headers)

# Vérifier le code de statut de la réponse
if response.status_code == 200:
    # Succès ! La requête a été traitée avec succès
    print("La requête POST a réussi !")
    print("Réponse du serveur:", response.json())
else:
    # Quelque chose s'est mal passé
    print("La requête POST a échoué. Code de statut:", response.status_code)
    print("Erreur :", response.text)
