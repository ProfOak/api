from app.core.db.db_model import Base, Restaurant, User
from app.core.db.db_manager import DBManager
from app.core.config_manager import ConfigManager
import pathlib


def createTables(engine):
    Base.metadata.create_all(engine)


def dropAllTables(engine):
    Base.metadata.drop_all(engine)


def populateRestaurants(session):
    r1 = Restaurant(name="Monop",
                    style="Supermarché",
                    city="Malakoff",
                    address="6 avenue Pierre Brossolette",
                    price=1
                    )
    r2 = Restaurant(name="Marguereat",
                    style="généraliste",
                    city="Malakoff",
                    address="X",
                    price=1
                    )
    r3 = Restaurant(name="Cocci Naan",
                    style="Kebab",
                    city="Malakoff",
                    address="56 avenue Pierre Brossolette",
                    price=2
                    )
    r4 = Restaurant(name="Kazoku",
                    style="Japonais",
                    city="Malakoff",
                    address="154 boulevard Gabriel Péri",
                    price=3
                    )
    r5 = Restaurant(name="Dominos Pizza",
                    style="Pizza",
                    city="Montrouge",
                    address="5 avenue de la Marne",
                    price=2
                    )
    r6 = Restaurant(name="Pizza hut",
                    style="Pizza",
                    city="Montrouge",
                    address="7 avenue de la Marne",
                    price=2
                    )
    r7 = Restaurant(name="Yoshiba",
                    style="Pizza",
                    city="Malakoff",
                    address="129 boulevard Gabriel Péri",
                    price=3
                    )
    r8 = Restaurant(name="Au fournil d'Enzo",
                    style="Boulangerie",
                    city="Malakoff",
                    address="83 avenue Pierre Brossolette",
                    price=2
                    )
    r9 = Restaurant(name="Boulangerie Patisserie Artisan",
                    style="Boulangerie",
                    city="Montrouge",
                    address="65 avenue Pierre Brossolette",
                    price=2
                    )
    
    session.add(r1)
    session.add(r2)
    session.add(r3)
    session.add(r4)
    session.add(r5)
    session.add(r6)
    session.add(r7)
    session.add(r8)
    session.add(r9)
    session.commit()
    session.close()


def main():
    confFilePath = pathlib.Path.cwd() / 'app' / 'conf' / 'config.ini'

    configManager = ConfigManager(confFilePath)
    dbManager = DBManager(configManager)
    engine = dbManager.getEngine()

    dropAllTables(engine)
    createTables(engine)

    populateRestaurants(dbManager.getSession())


if __name__ == "__main__":
    main()
